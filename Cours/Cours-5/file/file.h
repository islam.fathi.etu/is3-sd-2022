// file.h

#include <stdbool.h>

#define TMAX 4

struct file {
    double tab [TMAX];
    int re; // indice du prochain élément défilé
    int we; // indice du dernier élément défilé
};

// la file est vide si we = (re + 1) % TMAX
// la file est pleine si elle contient TMAX-1 éléments

extern void init_file (struct file*);
extern void clear_file (struct file*);
extern void enfiler (struct file*, double);
extern double defiler (struct file*);
extern bool est_vide_file (struct file*);




