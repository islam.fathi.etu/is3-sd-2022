#include <stdio.h>
#include "file.h"

int main ()
{   double T [] = { 23, -5, 37, 108 };
    int N = sizeof(T)/sizeof(double);

    struct file F;

    init_file (&F);
    for (int i = 0; i < N; i++)
    {
        enfiler (&F, T[i]);
        printf ("on vient d'enfiler %lf\n", T[i]);
    }
    while (! est_vide_file (&F))
    {
        double x;
        x = defiler (&F);
        printf ("on vient de défiler %lf\n", x);
    }
    clear_file (&F);
    return 0;
}

