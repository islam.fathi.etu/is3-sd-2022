// pile.h

#include <stdbool.h>

#define TAILLE_MAX 10

struct pile {
    double tab [TAILLE_MAX];
    int sp; // indice du sommet de pile. -1 si la pile est vide
};


extern void init_pile (struct pile*);
extern void clear_pile (struct pile*);
extern void empiler (struct pile*, double);
extern double depiler (struct pile*);
extern bool est_vide_pile (struct pile*);




