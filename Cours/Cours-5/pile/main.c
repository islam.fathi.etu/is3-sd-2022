#include <stdio.h>
#include "pile.h"

int main ()
{   double T [] = { 23, -5, 37, 108, 5, 78, -4, 0 };
    int N = sizeof(T)/sizeof(double);

    struct pile P;

    init_pile (&P);
    for (int i = 0; i < N; i++)
    {
        empiler (&P, T[i]);
        printf ("on vient d'empiler %lf\n", T[i]);
    }
    while (! est_vide_pile (&P))
    {
        double x;
        x = depiler (&P);
        printf ("on vient de dépiler %lf\n", x);
    }
    clear_pile (&P);
    return 0;
}

