// liste.h

/*
 * Implantation des listes chaînées de doubles.
 * Les maillons sont alloués dynamiquement.
 * Le champ suivant du dernier maillon vaut NIL.
 */

struct maillon 
  {
    double valeur;
    struct maillon* suivant;
  };

#define NIL (struct maillon*)0

/*
 * Le champ tete pointe vers le premier maillon de la liste.
 * Il vaut NIL si la liste est vide.
 * Le champ nbelem contient le nombre de maillon de la liste.
 */

struct liste 
  {
    struct maillon* tete;
    int nbelem;
  };

// Constructeur. Initialise à la liste vide
extern void init_liste (struct liste*);

// Destructeur
extern void clear_liste (struct liste*);

// Ajoute d (mode D) en tête de la liste L (mode D/R)
extern void ajout_en_tete (struct liste* L, double d);

// Imprime la liste (mode D)
extern void impression (struct liste);



