#include <stdio.h> 
#include <stdlib.h> 
#include <assert.h> 
#include <ctype.h>

int main ()
{   
    
    struct chaine;
    
    int c;
    char *s;
    int j;
    int k=0; //nombre de caractère lus
    int taille=4; // nombre de caractère alloué par s
    
    s=(char*)malloc(taille*sizeof(char)); //crée un tableau de 4 cases
    c=getchar();  //demande à l'utilisateur d'entrer une chaîne de caractère
    
    
    
    while(! isspace (c) ) // quand c est différente de espace
    { 
         if(k>taille){   
            taille=taille+8; 
            s=(char*)realloc(s,(taille)*sizeof(char));
        }
        //compter le nombre de caractères lus
        s[k]=c;
        c= getchar();
        k++;
    }
     
    for(j=0; j<k; j++){
        putchar(s[j]);
    }
    
    putchar('\n');
    free(s);
    return 0;
}

//printf("%s", s);
