

struct chaine 
{
    char * tab; // se termine par '\0'
    int i; //tab[i]='\0'
    int n;
    
};

extern void init_chaine( struct chaine*);

//destructeur
extern void clear_chaine (struct chaine* ); // extern facultatif 

// impression
extern void impression_chaine(struct chaine*);

// constructeur
extern void ajout_chaine(struct chaine*, char); // char
