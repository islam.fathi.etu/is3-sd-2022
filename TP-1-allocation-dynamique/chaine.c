/* chaine.c */

#include <stdio.h> /* pour printf */
#include "chaine.h"


/* Constructeur : initialiser la structure */

void init_chaine (struct chaine* s)   //s c'est le nom de l'objet : c'est un poiteur ce n'est pas la chaîne
{ 
    s->elements=(char*) 0;   //s->elements : s est un poniteur qui pointe vers le champs element de la structure
                             // initialiser element et les autres champs à 0
    s->taille=0; /*taille alloué */ 
    s->nb=0; /*nombre caractère lu */
}

/* Destructeur : libération de la mémoire */

void clear_chaine (struct chaine* s) 
{
    free(s->elements);  
}


/*imprimer chaine */

void print_chaine (struct chaine* s)
{
        printf ("%d\n", s->elements);

}


/* ajouter un element à la fin d'une chaine existance */

void add_char(struct chaine* s)
{
  if(s->taille==0){    //vérifier si la chaîne est vide
      s->elements = (char*)malloc(sizeof(char)); 
  } 
  
}


