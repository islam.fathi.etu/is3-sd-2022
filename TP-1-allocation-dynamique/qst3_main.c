#include <stdio.h>
#include <ctype.h>
#include "chaine.h"

int main() // lis les caractères un par un 
{
    struct chaine s;
    int c; // caractère
    init_chaine(&s);
    c=getchar();
    while(!isspace(c))
    {
        ajout_chaine(&s,c);
        c=getchar();
        
    }
    impression_chaine(&s);
    clear_chaine(&s);
}
