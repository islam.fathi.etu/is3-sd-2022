#include <stdio.h>
#include "abr1.h"

int main ()
{
    int cle [] = { 50, 25,75, 15, 30};
    int n = sizeof(cle)/sizeof(int);

    struct arbre* A;
    /*
    noeud ajouter_abr(int cle, abr);
    affichage_lineaire(abr);
    int hauteur_abr(abr);
    int nombre_noeuds_abr(abr);
    clear(abr);
    affichage_indent();
    bool rechercher_arbre(int cle , abr);
    */

    A = NIL;
    for (int i = 0; i < n; i++)
      {
        A = ajouter_arbre(A, cle[i]);
        affichage_croissant(A);
        printf("\n");
      }
      
    
    
    if (rechercher_arbre(A, 17))
      printf ("17 est dans l'ABR\n");
    else
      printf ("17 n'est pas dans l'ABR\n");
    clear_ABR (A);

    return 0;
}

