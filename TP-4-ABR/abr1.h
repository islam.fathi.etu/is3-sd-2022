// abr1.h
#include <stdbool.h>

#define NIL (struct arbre*)0

struct arbre 
  {
    struct arbre* gauche;
    int valeur;
    struct arbre* droite;
  };


extern void clear_ABR (struct arbre*);



extern bool rechercher_arbre (struct arbre*, int);

extern void affichage_croissant(struct arbre*);

extern int hauteur_arbre(struct arbre *);

extern struct arbre* ajouter_arbre(struct arbre*, int);

extern int nbr_noeud(struct arbre*A);
/*     <----------->
        type de retour
 */

