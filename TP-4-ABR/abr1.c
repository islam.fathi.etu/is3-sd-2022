// abr.c

#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include "abr1.h"

void clear_ABR (struct arbre* A)
{
    if(A != NIL){
        clear_ABR(A->gauche);
        clear_ABR(A->droite);
        free(A);
    }
}

bool rechercher_arbre (struct arbre* A, int cle)
{
    bool trouve = false;
    struct arbre* N;

    N = A;
    while (N != NIL && !trouve)
      {
        if (N->valeur == cle)
            trouve = true;
        else if (N->valeur < cle)
            N = N->droite;
        else
            N = N->gauche;
      }
    return trouve;
}



/* fonction auxiliaire de ajouter_ABR qui crée une feuille */

static struct arbre* new_feuille (int cle)
{
    struct arbre* F;
    F = (struct arbre*)malloc (sizeof (struct arbre));
    F->valeur = cle;
    F->gauche = NIL;
    F->droite = NIL;
    return F;
}


struct arbre* ajouter_arbre (struct arbre* A, int cle)
{
    if (A == NIL)
        return new_feuille (cle);
    else /* on sait que A n'est pas vide */
      {
        struct arbre *prec, *cour;
        prec = NIL; /* indéfini */
        cour = A;
     while (cour != NIL)
          {
            assert (cour->valeur != cle); /* la clé est unique ! */
        prec = cour; /* on mémorise l'ancienne valeur de cour */
           if (cour->valeur < cle)
                cour = cour->droite;
            else
                cour = cour->gauche;
          }
        /* cour vaut NIL et prec = le noeud à modifier */
        if (prec->valeur < cle)
            prec->droite = new_feuille (cle);
        else
            prec->gauche = new_feuille (cle);
        /* on retourne l'ABR complet modifié */
      return A;
      }
}


int hauteur_arbre(struct arbre *A){
    
    if(A == NIL)
        return 0;
  
    int gauche = hauteur_arbre(A->gauche);
    int droite = hauteur_arbre(A->droite);
  
    return ((gauche > droite ? gauche : droite) + 1);
}
    
void affichage_croissant(struct arbre*A){
    if (A != NIL){
        affichage_croissant(A->gauche);
        printf(" %d ", A->valeur);
        affichage_croissant(A->droite);
    }
}
    
int nbr_noeud(struct arbre*A)
{
	if( A == NIL) 
		return 0;
	else
		return 1 + nbr_noeud(A->gauche) + nbr_noeud(A->droite);
}

int somme(struct arbre* A)
{
   assert(A != NULL);
   return A->valeur + somme(A->gauche) + somme(A->droite);
}

void indente(struct arbre*A, int n){ 
   if (A!=NIL){ 
      indente(A->gauche, n+3);
      for( int i = 0; i< n; ++i){
          printf(" ");
          printf("%d", A->valeur);
      }
      indente(A->droite, n+3);
   }
}

void affichage_dot(struct arbre*A){
    
               

