        F. Boulier 2022
        Implantation de l'algorithme de Graham en C mais appelé depuis Python3

1. COMPILER LE CODE 

        Peut nécessiter d'installer python3-dev sous Linux
        Le résultat est une bibliothèque dynamique (.so) dans
        le répertoire ./lib/python

python3 setup.py install --home=.

2. EXÉCUTER L'EXEMPLE
   ==================

        Le préfixe "PYTHONPATH=./lib/python" affecte une valeur
        temporaire à la variable d'environnement PYTHONPATH : le
        chemin vers la bibliothèque (fichier .so)

PYTHONPATH=./lib/python python3 exemple.py

3. EXÉCUTER LE CODE C SOUS GDB
   ===========================

    - Supprimer (si nécessaire) l'ancienne installation

python3 setup.py clean --all

    - Compiler sans optimisation (moins O zéro)

CFLAGS="-O0" python3 setup.py install --home=.

    - Exécuter python sous gdb

PYTHONPATH=./lib/python gdb --args python3 exemple.py

        Sous gdb, il suffit alors de placer un point d'arrêt dans le 
        code C (break ma-fonction) et de lancer l'exécution (run)

4. Organisation du code

    - Le code C indépendant de Python :

        point.h
        point.c
        liste_point.h
        liste_point.c
        Graham.h
        Graham.c

    - Le code C qui implante le module geoalg et son type Graham

        geoalg.c


